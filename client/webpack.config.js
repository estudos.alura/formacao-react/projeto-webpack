const path = require('path');

module.exports = {
    entry: './app-src/app.js',
    output: {
        filename: 'bundle.js',
        // ('__dirname') => váriavel do nodejs que diz qual é o diretório atual desse módulo, ou seja, 
        // do módulo criado em webpack.config.js, onde esse módulo se encontra _
        path: path.resolve(__dirname, 'dist')
    },
    module: {
        rules: [
            {
                test: /\.js$/, // expressão regular informando ao webpack as extensões de arquivos que irá trabalhar
                exclude: /node_modules/,
                use: {
                    loader: 'babel-loader'
                }
            }
        ]
    }
}